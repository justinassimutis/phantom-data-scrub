var page = require('webpage').create(),
	address = "http://3test.savit.lt";

// bind console logging to the shell
page.onConsoleMessage = function (msg) { console.log(msg); };

// error tracking
page.onError = function(msg, trace) {
  var msgStack = ['ERROR: ' + msg];
  if (trace && trace.length) {
    msgStack.push('TRACE:');
    trace.forEach(function(t) {
      msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function +'")' : ''));
    });
  }
  console.error(msgStack.join('\n'));
};

page.open(address , function() {
  page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", function() {
  	console.log('jquery loaded');
	page.evaluate(function() {
	  console.log("bla");
	  console.log($(".nav-wrap.top li:first-child a").text());
	});
	phantom.exit()
  });
});