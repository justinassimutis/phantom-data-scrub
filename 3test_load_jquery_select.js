var page = require('webpage').create(),
	address = "http://3test.savit.lt";

// bind console logging to the shell
page.onConsoleMessage = function (msg) { console.log(msg); };

// error tracking
page.onError = function(msg, trace) {

  var msgStack = ['ERROR: ' + msg];

  if (trace && trace.length) {
    msgStack.push('TRACE:');
    trace.forEach(function(t) {
      msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function +'")' : ''));
    });
  }

  console.error(msgStack.join('\n'));

};

page.open(address, function(status) {
	if (status !== 'success') {
		console.log('FAIL to load the address');
	} else {
		console.log("bla");
		page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", function() {
			page.evaluate(function() {
				$(function () {
					console.log(document);
					console.log($(".nav-wrap.top li:first-child a").text());
				});
				
			});
		});
	}
	phantom.exit();
});


